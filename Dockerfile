FROM debian:latest
MAINTAINER antruziliak "antruziliak@gmail.com"
ENV REFRESHED_AT 2015-08-18
USER root
RUN apt-get -qq update
RUN apt-get install -y build-essential gawk texinfo pkg-config gettext automake libtool bison flex zlib1g-dev libgmp3-dev libmpfr-dev libmpc-dev git sudo module-init-tools cpio groff-base initramfs-tools linux-base dialog nano
RUN groupadd -r niam && useradd -s /bin/bash -m -p sasgd1GW7qeYY -r -g niam -G sudo niam
RUN echo 'niam ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
RUN mkdir -p /opt/xrmwrt && chown niam /opt/xrmwrt
ADD ./init.sh /init.sh
RUN chown niam /init.sh
USER niam
WORKDIR /opt
RUN git clone https://gitlab.com/Track/xrmwrt.git
WORKDIR /opt/xrmwrt
ADD https://gitlab.com/antruziliak/vm_xrmwrt/raw/master/.config /opt/xrmwrt/trunk/.config
USER root
RUN chown niam:niam /opt/xrmwrt/trunk/.config && chmod 644 /opt/xrmwrt/trunk/.config
USER niam
VOLUME ["/opt/xrmwrt/trunk/images"]
ENTRYPOINT ["/init.sh"]